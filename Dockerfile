# This file is a template, and might need editing before it works on your project.
FROM node:latest

RUN apt update && apt upgrade -y
RUN apt install g++ build-essential -y
RUN npm i -g node-gyp gulp
RUN git clone git://github.com/laverdet/node-fibers.git && cd node-fibers && npm i

CMD [ "node" ]
