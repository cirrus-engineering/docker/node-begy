# Docker Image with Node, Yarn, Gulp, and Build Essentials

Mainly used for building & deploying wordpress themes when using buddy, as the 
default Node image doesn't include `g++ and build-essentials` which the gulp commands need.